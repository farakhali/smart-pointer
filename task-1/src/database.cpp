#include <vector>
#include <map>
#include <memory>
#include "student.h"
#include "database.h"

using namespace emumba::training;
database::database()
{
    student s1("A", "A1", 1),
        s2("B", "A2", 2),
        s3("C", "A3", 3),
        s4("D", "A4", 4),
        s5("E", "A5", 5);

    student_data.push_back(s1);
    student_data.push_back(s2);
    student_data.push_back(s3);
    student_data.push_back(s4);
    student_data.push_back(s5);
}

std::shared_ptr<student> database::get_student_reference_shared(const std::string &student_name)
{
    std::vector<student>::iterator iter_start = student_data.begin();
    std::vector<student>::iterator iter_end = student_data.end();
    for (iter_start; iter_start != iter_end; ++iter_start)
    {
        if (student_name == iter_start->get_name())
        {
            return std::make_shared<student>(*iter_start);
        }
    }
    return nullptr;
}

std::unique_ptr<student> database::get_student_reference_unique(const std::string &student_name)
{
    std::vector<student>::iterator iter_start = student_data.begin();
    std::vector<student>::iterator iter_end = student_data.end();
    for (iter_start; iter_start != iter_end; ++iter_start)
    {
        if (student_name == iter_start->get_name())
        {
            return std::make_unique<student>(*iter_start);
        }
    }
    return nullptr;
}
