#include <string>
#include <map>
#include <iostream>
#include "student.h"

using namespace emumba::training;

student::student()
{
    set_roll_no("roll_no");
    set_age(0);
    set_name("name");
}
student::~student() {}

student::student(const std::string &name, const std::string &roll_no, const int &age)
{
    set_roll_no(roll_no);
    set_age(age);
    set_name(name);
}

const std::string student::get_name()
{
    return student_record.name;
}
void student::set_name(const std::string &name)
{
    student_record.name = name;
}

const int student::get_age()
{
    return student_record.age;
}
void student::set_age(const int &age)
{
    student_record.age = age;
}

const std::string student::get_roll_no()
{
    return student_record.roll_number;
}
void student::set_roll_no(const std::string &roll_no)
{
    student_record.roll_number = roll_no;
}

const void student::print_data()
{
    std::cout << "Name : " << get_name() << "\t"
              << "Roll No. : " << get_roll_no() << "\t"
              << "Age : " << get_age() << "\n";
}
