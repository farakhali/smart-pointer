#ifndef _DATABASE_H
#define _DATABASE_H

#include <map>
#include <vector>
#include <memory>
#include <iostream>
#include "student.h"

namespace emumba::training
{
    class database
    {
    private:
        std::vector<student> student_data;

    public:
        database();
        std::shared_ptr<student> get_student_reference_shared(const std::string &student_name);
        std::unique_ptr<student> get_student_reference_unique(const std::string &student_name);
    };
}

#endif