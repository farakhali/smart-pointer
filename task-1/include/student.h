#ifndef _STUDENT_H
#define _STUDENT_H

#include <string>
#include <map>

namespace emumba::training
{
    class student
    {
    private:
        struct student_record
        {
            std::string name;
            std::string roll_number;
            int age;
        } student_record;

    public:
        student();
        ~student();

        student(const std::string &name, const std::string &roll_no, const int &age);

        const std::string get_name();
        void set_name(const std::string &name);

        const int get_age();
        void set_age(const int &age);

        const std::string get_roll_no();
        void set_roll_no(const std::string &roll_no);

        const void print_data();
    };
}
#endif