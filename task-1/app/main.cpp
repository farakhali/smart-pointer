#include <iostream>
#include "student.h"
#include "database.h"

using namespace emumba::training;

void print_shared_data(const std::shared_ptr<student> &shared_ptr);
void print_unique_data(const std::unique_ptr<student> &unique_ptr);

int main()
{
    database student_data;
    std::shared_ptr<student> share_ptr = student_data.get_student_reference_shared("A");
    print_shared_data(share_ptr);

    std::unique_ptr<student> unique_ptr = student_data.get_student_reference_unique("E");
    print_unique_data(unique_ptr);

    return 0;
}

void print_shared_data(const std::shared_ptr<student> &shared_ptr)
{
    std::cout << "share_ptr Fun ==>\t";
    if (shared_ptr != nullptr)
    {
        shared_ptr->print_data();
    }
    else
        std::cout << "Student Not Found\n";
}

void print_unique_data(const std::unique_ptr<student> &unique_ptr)
{
    std::cout << "unique_ptr Fun ==>\t";
    if (unique_ptr != nullptr)
        unique_ptr->print_data();
    else
        std::cout << "Student Not Found\n";
}
