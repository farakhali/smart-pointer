# smart-pointer

This task mainly focus on the use of smart pointers(unique ad shared pointers) how to access and use them.

## How to Run Code

In this app we added data of some students using constructor of database class and then by using shared and unique pointers we access students data. To build the main app follow the following commands

---

`git clone https://gitlab.com/farakhali/smart-pointer.git`  
`cd smart-pointer`  
`cd task-1`  
`cmake -DDEBUG=ON/OFF ..`  
`make`

---

- Run main executable using `./main`
